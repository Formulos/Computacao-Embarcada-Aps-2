#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/************************************************************************/
/* defines                                                              */
/************************************************************************/


#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

#define LED_PIO PIOB
#define LED_PIO_ID ID_PIOB
#define LED_PIO_PIN 2
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUZZ_PIO PIOD
#define BUZZ_PIO_ID ID_PIOD
#define BUZZ_PIO_PIN 22
#define BUZZ_PIO_PIN_MASK (1 << BUZZ_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 4
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

// Encoder decoder
#define EN_CLK PIOD // Connected to CLK on KY040 encoder
#define EN_CLK_ID ID_PIOD
#define EN_CLK_PIN 11
#define EN_CLK_PIN_MASK (1 <<  EN_CLK_PIN)

#define EN_DT PIOD // Connected to DT on KY040 encoder
#define EN_DT_ID ID_PIOD
#define EN_DT_PIN 26
#define EN_DT_PIN_MASK (1 <<  EN_DT_PIN)

void liga_led();
void desliga_led();
void toca_buzz();
void desliga_buzz();
void sing();
void buzz();

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

volatile int encoderPosCount = 0;
int number_of_songs = 4;

int melody[] = {
	NOTE_E7, NOTE_E7, 0, NOTE_E7,
	0, NOTE_C7, NOTE_E7, 0,
	NOTE_G7, 0, 0,  0,
	NOTE_G6, 0, 0, 0,
	
	NOTE_C7, 0, 0, NOTE_G6,
	0, 0, NOTE_E6, 0,
	0, NOTE_A6, 0, NOTE_B6,
	0, NOTE_AS6, NOTE_A6, 0,
	
	NOTE_G6, NOTE_E7, NOTE_G7,
	NOTE_A7, 0, NOTE_F7, NOTE_G7,
	0, NOTE_E7, 0, NOTE_C7,
	NOTE_D7, NOTE_B6, 0, 0,
	
	NOTE_C7, 0, 0, NOTE_G6,
	0, 0, NOTE_E6, 0,
	0, NOTE_A6, 0, NOTE_B6,
	0, NOTE_AS6, NOTE_A6, 0,
	
	NOTE_G6, NOTE_E7, NOTE_G7,
	NOTE_A7, 0, NOTE_F7, NOTE_G7,
	0, NOTE_E7, 0, NOTE_C7,
	NOTE_D7, NOTE_B6, 0, 0
};

int tempo[] = {
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	9, 9, 9,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	9, 9, 9,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
};
//Underworld melody
int underworld_melody[] = {
	NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
	NOTE_AS3, NOTE_AS4, 0,
	0,
	NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
	NOTE_AS3, NOTE_AS4, 0,
	0,
	NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
	NOTE_DS3, NOTE_DS4, 0,
	0,
	NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
	NOTE_DS3, NOTE_DS4, 0,
	0, NOTE_DS4, NOTE_CS4, NOTE_D4,
	NOTE_CS4, NOTE_DS4,
	NOTE_DS4, NOTE_GS3,
	NOTE_G3, NOTE_CS4,
	NOTE_C4, NOTE_FS4, NOTE_F4, NOTE_E3, NOTE_AS4, NOTE_A4,
	NOTE_GS4, NOTE_DS4, NOTE_B3,
	NOTE_AS3, NOTE_A3, NOTE_GS3,
	0, 0, 0
};
//Underwolrd tempo
int underworld_tempo[] = {
	12, 12, 12, 12,
	12, 12, 6,
	3,
	12, 12, 12, 12,
	12, 12, 6,
	3,
	12, 12, 12, 12,
	12, 12, 6,
	3,
	12, 12, 12, 12,
	12, 12, 6,
	6, 18, 18, 18,
	6, 6,
	6, 6,
	6, 6,
	18, 18, 18, 18, 18, 18,
	10, 10, 10,
	10, 10, 10,
	3, 3, 3
};
int starwars_melody[] = {
	NOTE_A4, NOTE_A4, NOTE_A4, NOTE_F4,
	NOTE_C5, NOTE_A4, NOTE_F4, NOTE_C5,
	NOTE_A4, 0, //DELAY 500
	NOTE_E5, NOTE_E5, NOTE_E5, NOTE_F5,
	NOTE_C5, NOTE_G5, NOTE_F4, NOTE_C5,
	NOTE_A4, 0,//DELAY 500
	//second
	NOTE_A5, NOTE_A4, NOTE_A4, NOTE_A5,
	NOTE_GS5, NOTE_G5, NOTE_FS5, NOTE_F5,
	NOTE_FS5, 0, NOTE_AS4, NOTE_DS5, NOTE_D5,
	NOTE_CS5, NOTE_C5, NOTE_B4, NOTE_C5, 0,
	//variant 1
	NOTE_F4, NOTE_GS4, NOTE_F4, NOTE_A4,
	NOTE_C5, NOTE_A4, NOTE_C5,
	NOTE_E5, 0,
	//second
	NOTE_A5, NOTE_A4, NOTE_A4, NOTE_A5,
	NOTE_GS5, NOTE_G5, NOTE_FS5, NOTE_F5,
	NOTE_FS5, 0, NOTE_AS4, NOTE_DS5, NOTE_D5,
	NOTE_CS5, NOTE_C5, NOTE_B4, NOTE_C5, 0,
	//variant 2
	NOTE_F4, NOTE_GS4, NOTE_F4, NOTE_C5,
	NOTE_A4, NOTE_F4, NOTE_C5, NOTE_A4,
	0
};
int starwars_tempo[] = {
	50, 50, 50, 35,
	15, 50, 35, 15,
	65, 50, 50, 50,
	50, 35, 15, 50,
	35, 15, 65, 50,
	//second
	50, 30, 15, 50,
	32, 17, 12, 12,
	25, 32, 25, 50,
	32, 17, 12, 12,
	25, 35,
	//variante 1
	25, 50, 35, 12,
	50, 37, 12, 65,
	50,
	//second
	50, 30, 15, 50,
	32, 17, 12, 12,
	25, 32, 25, 50,
	32, 17, 12, 12,
	25, 35,
	//variante 2
	25, 50, 37, 12,
	50, 37, 12, 65,
	65
};


int ptc[] = { 
	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
	NOTE_A4, NOTE_G4, NOTE_A4, 0,
	
	NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0,
	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
	NOTE_A4, NOTE_G4, NOTE_A4, 0,
	
	NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0,
	NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0,
	NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0,
	NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0,
	
	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_D5, NOTE_E5, NOTE_A4, 0,
	NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0,
	NOTE_C5, NOTE_A4, NOTE_B4, 0,

	NOTE_A4, NOTE_A4,

	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
	NOTE_A4, NOTE_G4, NOTE_A4, 0,

	NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0,
	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
	NOTE_A4, NOTE_G4, NOTE_A4, 0,
	
	NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0,
	NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0,
	NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0,
	NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0,
	
	NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0,
	NOTE_D5, NOTE_E5, NOTE_A4, 0,
	NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0,
	NOTE_C5, NOTE_A4, NOTE_B4, 0,


	NOTE_E5, 0, 0, NOTE_F5, 0, 0,
	NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0,
	NOTE_D5, 0, 0, NOTE_C5, 0, 0,
	NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4,

	NOTE_E5, 0, 0, NOTE_F5, 0, 0,
	NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0,
	NOTE_D5, 0, 0, NOTE_C5, 0, 0,
	NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4
};

int ptc_tempo[] = { 
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 125,
	
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 125,
	
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 125, 250, 125,

	125, 125, 250, 125, 125,
	250, 125, 250, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 375,

	250, 125,

	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 125,
	
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 125,
	
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 250, 125, 125,
	125, 125, 125, 250, 125,

	125, 125, 250, 125, 125,
	250, 125, 250, 125,
	125, 125, 250, 125, 125,
	125, 125, 375, 375,

	
	250, 125, 375, 250, 125, 375,
	125, 125, 125, 125, 125, 125, 125, 125, 375,
	250, 125, 375, 250, 125, 375,
	125, 125, 125, 125, 125, 500,

	250, 125, 375, 250, 125, 375,
	125, 125, 125, 125, 125, 125, 125, 125, 375,
	250, 125, 375, 250, 125, 375,
	125, 125, 125, 125, 125, 500
};



volatile uint8_t fall = true;
uint8_t ign = true;



/************************************************************************/
/* funcoes briza                                                        */
/************************************************************************/

int song = 0;

void sing(int s) {

	song = s;
	if (song == 3) {
		int size = sizeof(underworld_melody) / sizeof(int);
		for (int thisNote = 0; thisNote < size; thisNote++) {
			int noteDuration = 1000 / underworld_tempo[thisNote];
			buzz(underworld_melody[thisNote], noteDuration);
			int pauseBetweenNotes = noteDuration * 1.30;
			delay_ms(pauseBetweenNotes);
			buzz(0, noteDuration);	
		}	
	} 
	
	if (song == 4) {
		int size = sizeof(starwars_melody) / sizeof(int);
		for (int thisNote = 0; thisNote < size; thisNote++) {
		
			int noteDuration = 10000 / starwars_tempo[thisNote];			
			buzz(starwars_melody[thisNote], noteDuration);
			int pauseBetweenNotes = noteDuration;
			delay_ms(pauseBetweenNotes);
			buzz(0, noteDuration);
		}
	}
	
	if (song == 1) {
		int size = sizeof(melody) / sizeof(int);
		for (int thisNote = 0; thisNote < size; thisNote++) {
			
			int noteDuration = 1000 / tempo[thisNote];
			buzz(melody[thisNote], noteDuration);
			int pauseBetweenNotes = noteDuration * 1.30;
			delay_ms(pauseBetweenNotes);
			buzz(0, noteDuration);
		}
	}
	
	if (song == 2){
		int size = sizeof(ptc) / sizeof(int);
		for (int thisNote = 0; thisNote < size; thisNote++) {
			
			int noteDuration = 10000 / ptc_tempo[thisNote];
			buzz(ptc[thisNote], noteDuration);
			float pauseBetweenNotes = noteDuration * 1.8;
			delay_ms(pauseBetweenNotes);
			buzz(0, noteDuration);
		}
	}
}

void buzz(long frequency, long length) {
	long delayValue = 1000000 / frequency / 2;
	long numCycles = frequency * length / 1000; 
	for (long i = 0; i < numCycles; i++) { 
		pio_set(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
		delay_us(delayValue);
		pio_clear(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
		delay_us(delayValue);
	}
}

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

void but_callBack(void){
	if(ign != true){
		if(fall){
			liga_led();
			
			pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but_callBack);
			
			if (encoderPosCount > (number_of_songs-1)){ //as musicas comecam no 1
				encoderPosCount = number_of_songs - 1;
			}
			if (encoderPosCount < 0){
				encoderPosCount = 0;
			}
			sing(1 + encoderPosCount); 
			fall = false;
		}
		else{
			desliga_led();
			pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but_callBack);
			fall = true;
		}
		
	}
	ign = false;
}

void liga_led(){
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
}
void desliga_led(){
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
}
void toca_buzz(){
	for (int i = 0; i < 200; i++)
	{
		pio_clear(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
		delay_ms(10);
		pio_set(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
		delay_ms(10);
	}
		
	
}

void desliga_buzz(){
	pio_set(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
}

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){

	//set clock
	sysclk_init();
	
	//Desliga reinicialização indesejada
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	//Ligar o PIOC que vai controlar o LED
	
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	pmc_enable_periph_clk(EN_CLK_ID);
	pmc_enable_periph_clk(EN_DT_ID);
	
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_configure(BUZZ_PIO, PIO_OUTPUT_0, BUZZ_PIO_PIN_MASK, PIO_DEFAULT);

	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but_callBack);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
	pio_set(BUZZ_PIO, BUZZ_PIO_PIN_MASK);
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	

	
	
	volatile int pinALast = 0;
	volatile int aVal = 0;
	volatile int bCW = 0;
	
	pio_configure(EN_CLK, PIO_INPUT,  EN_CLK_PIN_MASK, PIO_DEFAULT);
	pio_configure(EN_DT, PIO_INPUT,  EN_DT_PIN_MASK, PIO_DEFAULT);

	pinALast = pio_get(EN_CLK, PIO_INPUT,  EN_CLK_PIN_MASK); 

	
	//acaba o encoder decoder aqui
	
	
	while (1) {

	aVal = pio_get(EN_CLK, PIO_INPUT,  EN_CLK_PIN_MASK);
	if (aVal != pinALast){ 
		if (pio_get(PIOD, PIO_INPUT,  EN_DT_PIN_MASK)!= aVal){ 
			encoderPosCount++;
			bCW = true;
			}
			else {
			bCW = false;
			encoderPosCount--;
		}
		
		if (bCW){
			
			pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(5);
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
		}
		else{
			
			pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(5);
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
		}
		
		pinALast = aVal;
	}
	

	}
	
	return 0;
}